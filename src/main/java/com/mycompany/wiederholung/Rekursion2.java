/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wiederholung;

/**
 *
 * @author Info3A Senoner Nils
 */
public class Rekursion2 {

    public static void main(String[] args) {

        System.out.println(Ffunktion(6));
    }

    static int Ffunktion(int n) {
        if (n == 1) {
            return 1;
        } else {
            return Ffunktion(n - 1) + 2 * n - 1;
        }

    }

}
