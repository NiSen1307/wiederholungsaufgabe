package com.mycompany.wiederholung;

import javax.swing.JOptionPane;

/**
 *
 * @author Info3A Senoner Nils
 */
public class Aufgabe2 {

    public static void main(String[] args) {
        int Schüler = Integer.parseInt(  JOptionPane.showInputDialog("Wie viele Schüler fahren mit?"));
        int Begleiter = Integer.parseInt(  JOptionPane.showInputDialog("Wie viele Begleitpersonen fahren mit?"));
        double kilometer = Double.parseDouble(  JOptionPane.showInputDialog("Wie viele Kilometer fahren sie?"));
        double freifahrtenpr = 0;
        double begleiterpreis = ((kilometer*0.05)/2)*Begleiter;
        double schülerpreis = (Schüler * 0.05) * kilometer;
        double durchschnittkosten = kilometer*0.05;
        if (Schüler>10){
            int freifahrt = Schüler /10;
            freifahrtenpr = (durchschnittkosten*freifahrt);
        }
        schülerpreis = schülerpreis - freifahrtenpr;
        System.out.println("Der Gesamnt Schüler Preis ist: "+schülerpreis+" Der Preis Für Begleiter ist :"+begleiterpreis
                            +" Der Preis für 1 Schüler ist: "+durchschnittkosten);
        
    }
}
