/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wiederholung;

/**
 *
 * @author Info3A Senoner Nils
 */
public class Rekursion1 {

    public static void main(String[] args) {
        System.out.println(summe(0, 4));
        System.out.println(multi(5, 4));

    }

    static int summe(int a, int b) {
        if (b <= 1) {
            return a + 1;
        } else {
            return summe(a + 1, b - 1);
        }
    }

    static int multi(int a, int b) {

        if (b <= 1) {
            return a;
        } else {

            return a + multi(a, b - 1);
        }

    }

}
