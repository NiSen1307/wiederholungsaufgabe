
        
package com.mycompany.wiederholung;
import javax.swing.JOptionPane;
/**
 *
 * @author Info3A Senoner Nils
 */
public class Aufgabe1 {
    public static void main(String[] args){
        double pi = Math.PI;
        double r = Double.parseDouble(  JOptionPane.showInputDialog("Radius des Kreises:"));
        double umf = 2*pi*r;
        System.out.println("Der Umfang des Kreises ist: "+umf);
        double fläche = r*(umf/2);
        System.out.println("Die Fläche des Kreises ist: "+fläche);
    }
    

}
