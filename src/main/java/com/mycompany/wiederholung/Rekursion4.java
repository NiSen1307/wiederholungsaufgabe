/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.wiederholung;

/**
 *
 * @author Info3A Senoner Nils
 */
public class Rekursion4 {

    public static void main(String[] args) {

        System.out.println(quer(123456));
    }

    static int quer(int zahl) {
        if (zahl <= 9) {
            return zahl;
        } else {
            return zahl % 10 + quer(zahl / 10);
        }
    }

}
